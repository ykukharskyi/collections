package org.kpi.algorithms.collections.stereotypes;

public interface Map<K, V> {
    V put(K key, V val);

    V get(K key);
}
