package org.kpi.algorithms.collections.redblacktreemap;

public class Node<K, V> {
    private K key;
    private V val;
    private Node<K, V> parent;
    private Node<K, V> left;
    private Node<K, V> right;
    private boolean red;
    private int size;

    Node(K key, V val, boolean red) {
        this.key = key;
        this.val = val;
        this.red = red;
    }

    public Node(K key, V val, Node<K, V> parent, boolean red) {
        this.key = key;
        this.val = val;
        this.parent = parent;
        this.red = red;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getVal() {
        return val;
    }

    public void setVal(V val) {
        this.val = val;
    }

    public Node<K, V> getParent() {
        return parent;
    }

    public void setParent(Node<K, V> parent) {
        this.parent = parent;
    }

    public Node<K, V> getLeft() {
        return left;
    }

    public void setLeft(Node<K, V> left) {
        this.left = left;
    }

    public Node<K, V> getRight() {
        return right;
    }

    public void setRight(Node<K, V> right) {
        this.right = right;
    }

    public boolean isRed() {
        return red;
    }

    public boolean isBlack() {
        return !red;
    }

    public void setRed(boolean red) {
        this.red = red;
    }

    public void setColor(boolean red) {
        this.red = red;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
