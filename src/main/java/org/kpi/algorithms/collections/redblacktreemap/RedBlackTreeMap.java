package org.kpi.algorithms.collections.redblacktreemap;

import org.kpi.algorithms.collections.stereotypes.Map;

public class RedBlackTreeMap<K extends Comparable, V> implements Map<K, V> {
    private static final boolean RED = true;
    private static final boolean BLACK = false;

    private Node<K, V> root;

    @Override
    public V put(K key, V val) {
        if (root == null) {
            root = new Node<>(key, val, BLACK);
        } else {
            Node<K, V> current = root;
            while (current != null) {
                if (key.compareTo(current.getKey()) > 0) {
                    if (current.getRight() == null) {
                        Node<K, V> node = new Node<>(key, val, current, RED);
                        current.setRight(node);
                        fixTree(node);
                        break;
                    } else {
                        current = current.getRight();
                    }
                } else if (key.compareTo(current.getKey()) < 0) {
                    if (current.getLeft() == null) {
                        Node<K, V> node = new Node<>(key, val, current, RED);
                        current.setLeft(node);
                        fixTree(node);
                        break;
                    } else {
                        current = current.getLeft();
                    }
                } else {
                    current.setKey(key);
                    current.setVal(val);
                    break;
                }
            }
        }
        return val;
    }

    @Override
    public V get(K key) {
        V val = null;
        if (root != null) {
            Node<K, V> next = root;
            while (next != null) {
                if (key.compareTo(next.getKey()) == 0) {
                    val = next.getVal();
                    break;
                } else if (key.compareTo(next.getKey()) > 0) {
                    next = next.getRight();
                } else if (key.compareTo(next.getKey()) < 0) {
                    next = next.getLeft();
                }
            }
        }
        return val;
    }

    void fixTree(Node<K, V> node) {
        while (node.getParent().isRed()) {
            if (node.getParent() == node.getParent().getParent().getLeft()) {
                Node<K, V> uncle = node.getParent().getParent().getRight();
                if (uncle != null && uncle.isRed()) {
                    node.getParent().setColor(BLACK);
                    uncle.setColor(BLACK);
                    node.getParent().getParent().setColor(RED);
                    node = node.getParent().getParent();
                    continue;
                }
                if (node == node.getParent().getRight()) {
                    node = node.getParent();
                    rotateLeft(node);
                }
                node.getParent().setColor(BLACK);
                node.getParent().getParent().setColor(RED);
                rotateRight(node.getParent().getParent());
            } else {
                Node<K, V> uncle = node.getParent().getParent().getLeft();
                if (uncle != null && uncle.isRed()) {
                    node.getParent().setColor(BLACK);
                    uncle.setColor(BLACK);
                    node.getParent().getParent().setColor(RED);
                    node = node.getParent().getParent();
                    continue;
                }
                if (node == node.getParent().getLeft()) {
                    node = node.getParent();
                    rotateRight(node);
                }
                node.getParent().setColor(BLACK);
                node.getParent().getParent().setColor(RED);
                rotateLeft(node.getParent().getParent());
            }
        }
        root.setColor(BLACK);
    }

    private void rotateLeft(Node<K, V> node) {
        if (node.getParent() != null) {
            if (node == node.getParent().getLeft()) {
                node.getParent().setLeft(node.getRight());
            } else {
                node.getParent().setRight(node.getRight());
            }
            node.getRight().setParent(node.getParent());
            node.setParent(node.getRight());
            if (node.getRight().getLeft() != null) {
                node.getRight().getLeft().setParent(node);
            }
            node.setRight(node.getRight().getLeft());
            node.getParent().setLeft(node);
        } else {
            Node<K, V> right = root.getRight();
            root.setRight(right.getLeft());
            right.getLeft().setParent(root);
            root.setParent(right);
            right.setLeft(root);
            right.setParent(null);
            root = right;
        }
    }

    private void rotateRight(Node<K, V> node) {
        if (node.getParent() != null) {
            if (node == node.getParent().getLeft()) {
                node.getParent().setLeft(node.getLeft());
            } else {
                node.getParent().setRight(node.getLeft());
            }
            node.getLeft().setParent(node.getParent());
            node.setParent(node.getLeft());
            if (node.getLeft().getRight() != null) {
                node.getLeft().getRight().setParent(node);
            }
            node.setLeft(node.getLeft().getRight());
            node.getParent().setRight(node);
        } else {
            Node<K, V> left = root.getLeft();
            root.setLeft(root.getLeft().getRight());
            left.getRight().setParent(root);
            root.setParent(left);
            left.setRight(root);
            left.setParent(null);
            root = left;
        }
    }

    Node<K, V> getRoot() {
        return root;
    }

    void setRoot(Node<K, V> root) {
        this.root = root;
    }
}
