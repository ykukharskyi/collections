package org.kpi.algorithms.collections.redblackhashmap;

import org.kpi.algorithms.collections.redblacktreemap.RedBlackTreeMap;
import org.kpi.algorithms.collections.stereotypes.Map;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

public class RedBlackHashMap<K extends Comparable, V> implements Map<K, V> {

    private Map<K, V>[] buckets;

    public RedBlackHashMap(int capacity) {
        this.buckets = new RedBlackTreeMap[capacity];
    }

    @Override
    public V put(K key, V val) {
        requireNonNull(key, "Key should be not null");
        findOrCreateBucket(key).put(key, val);
        return val;
    }

    @Override
    public V get(K key) {
        return ofNullable(buckets[calculateBucketIndex(key)])
                .map(bucket -> bucket.get(key))
                .orElse(null);
    }

    Map<K, V>[] getBuckets() {
        return buckets;
    }

    void setBuckets(Map<K, V>[] buckets) {
        this.buckets = buckets;
    }

    Map<K, V> createBucket() {
        return new RedBlackTreeMap<>();
    }

    private Map<K, V> findOrCreateBucket(K key) {
        int index = calculateBucketIndex(key);
        Map<K, V> bucket = buckets[index];
        if (bucket == null) {
            bucket = createBucket();
            buckets[index] = bucket;
        }
        return bucket;
    }

    private int calculateBucketIndex(K key) {
        int hash = Math.abs(key.hashCode());
        return hash % buckets.length;
    }
}
