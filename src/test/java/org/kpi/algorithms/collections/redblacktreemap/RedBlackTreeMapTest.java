package org.kpi.algorithms.collections.redblacktreemap;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class RedBlackTreeMapTest {

    private RedBlackTreeMap<Integer, Object> testedEntry = spy(new RedBlackTreeMap<>());

    private Integer key = 100;

    private Object val = createVal();

    private Node<Integer, Object> root = createNode(key, val);

    @Before
    public void setUp() {
        testedEntry.setRoot(root);
    }

    @Test
    public void shouldReturnNullWhenRootIsNull() {
        testedEntry.setRoot(null);
        assertThat(testedEntry.get(key)).isNull();
    }

    @Test
    public void shouldReturnNullWhenKeyIsAbsent() {
        testedEntry.setRoot(createNode(key, val));
        assertThat(testedEntry.get(300)).isNull();
    }

    @Test
    public void shouldReturnAddedValue() {
        testedEntry.setRoot(null);
        assertThat(testedEntry.put(key, val)).isSameAs(val);
    }

    @Test
    public void shouldCreateRoot() {
        testedEntry.setRoot(null);
        testedEntry.put(key, val);
        assertThat(testedEntry.getRoot()).isNotNull().satisfies(node -> {
            assertThat(node.getKey()).isSameAs(key);
            assertThat(node.getVal()).isSameAs(val);
            assertThat(node.isRed()).isFalse();
            assertThat(node.getParent()).isNull();
        });
        verify(testedEntry, never()).fixTree(any());
    }

    @Test
    public void shouldReturnValue() {
        assertThat(testedEntry.get(key)).isSameAs(val);
    }

    @Test
    public void shouldReturnRightValue() {
        Object rightVal = createVal();
        root.setRight(createNode(200, rightVal));
        assertThat(testedEntry.get(200)).isSameAs(rightVal);
    }

    @Test
    public void shouldReturnLeftValue() {
        Object leftVal = createVal();
        root.setLeft(createNode(50, leftVal));
        assertThat(testedEntry.get(50)).isSameAs(leftVal);
    }

    @Test
    public void shouldAddRight() {
        Object newVal = createVal();
        testedEntry.put(200, newVal);
        assertThat(testedEntry.getRoot()).isNotNull().satisfies(root -> {
            assertThat(root.getRight()).isNotNull().satisfies(right -> {
                assertThat(right.getKey()).isEqualTo(200);
                assertThat(right.getVal()).isSameAs(newVal);
                assertThat(right.isRed()).isTrue();
                assertThat(right.getParent()).isSameAs(root);
                verify(testedEntry).fixTree(right);
            });
        });
    }

    @Test
    public void shouldAddLeft() {
        Object newVal = createVal();
        testedEntry.put(50, newVal);
        assertThat(testedEntry.getRoot()).isNotNull().satisfies(root -> {
            assertThat(root.getLeft()).isNotNull().satisfies(left -> {
                assertThat(left.getKey()).isEqualTo(50);
                assertThat(left.getVal()).isSameAs(newVal);
                assertThat(left.isRed()).isTrue();
                assertThat(left.getParent()).isSameAs(root);
                verify(testedEntry).fixTree(same(left));
            });
        });
    }

    @Test
    public void shouldAddRightNodeToRightSecondNode() {
        Node<Integer, Object> secondNode = createNode(200, createVal());
        root.setRight(secondNode);
        Object newVal = createVal();
        testedEntry.put(300, newVal);
        assertThat(secondNode.getRight()).isNotNull().satisfies(right -> {
            assertThat(right.getKey()).isEqualTo(300);
            assertThat(right.getVal()).isSameAs(newVal);
            assertThat(right.isRed()).isTrue();
            assertThat(right.getParent()).isSameAs(secondNode);
            verify(testedEntry).fixTree(same(right));
        });
    }

    @Test
    public void shouldAddLeftNodeToLeftSecondNode() {
        Node<Integer, Object> secondNode = createNode(50, createVal());
        root.setLeft(secondNode);
        Object newVal = createVal();
        testedEntry.put(20, newVal);
        assertThat(secondNode.getLeft()).isNotNull().satisfies(left -> {
            assertThat(left.getKey()).isEqualTo(20);
            assertThat(left.getVal()).isSameAs(newVal);
            assertThat(left.isRed()).isTrue();
            assertThat(left.getParent()).isSameAs(secondNode);
            verify(testedEntry).fixTree(same(left));
        });
    }

    @Test
    public void shouldAddRightNodeToLeftSecondNode() {
        Node<Integer, Object> secondNode = createNode(50, createVal());
        root.setLeft(secondNode);
        Object newVal = createVal();
        testedEntry.put(70, newVal);
        assertThat(secondNode.getRight()).isNotNull().satisfies(right -> {
            assertThat(right.getKey()).isEqualTo(70);
            assertThat(right.getVal()).isSameAs(newVal);
            assertThat(right.isRed()).isTrue();
            assertThat(right.getParent()).isSameAs(secondNode);
            verify(testedEntry).fixTree(same(right));
        });
    }

    @Test
    public void shouldReplaceValue() {
        Object newVal = createVal();
        testedEntry.put(100, newVal);
        assertThat(testedEntry.getRoot()).isNotNull().satisfies(root -> {
            assertThat(root.getKey()).isEqualTo(100);
            assertThat(root.getVal()).isEqualTo(newVal);
        });
        verify(testedEntry, never()).fixTree(any());
    }

    private <K, V> Node<K, V> createNode(K key, V val) {
        return new Node<>(key, val, false);
    }

    private Object createVal() {
        return new Object();
    }

}