package org.kpi.algorithms.collections.redblackhashmap;

import org.junit.Before;
import org.junit.Test;
import org.kpi.algorithms.collections.stereotypes.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

public class RedBlackHashMapTest {
    private static final int CAPACITY = 3;

    private RedBlackHashMap<Integer, Object> testedEntry = spy(new RedBlackHashMap<>(CAPACITY));

    private Integer key = 100;

    private Object val = createVal();

    private Map<Integer, Object> bucket1 = mock(Map.class);

    private Map<Integer, Object> bucket2 = mock(Map.class);

    private Map<Integer, Object> bucket3 = mock(Map.class);

    @Before
    public void setUp() {
        testedEntry.setBuckets(new Map[]{bucket1, bucket2, bucket3});
    }

    @Test
    public void shouldCreateBuckets() {
        testedEntry = new RedBlackHashMap<>(10);
        assertThat(testedEntry.getBuckets())
                .isNotNull()
                .hasSize(10);
    }

    @Test
    public void shouldThrowNullPointerException() {
        assertThatThrownBy(() -> testedEntry.put(null, val))
                .isInstanceOf(NullPointerException.class)
                .hasMessage("Key should be not null");
    }

    @Test
    public void shouldReturnValue() {
        assertThat(testedEntry.put(key, val)).isSameAs(val);
    }

    @Test
    public void shouldPutToBucket1() {
        key = 3;
        testedEntry.put(key, val);
        verify(bucket1).put(key, val);
    }

    @Test
    public void shouldPutToBucket2() {
        key = 1;
        testedEntry.put(key, val);
        verify(bucket2).put(key, val);
    }

    @Test
    public void shouldPutToBucket3() {
        key = 2;
        testedEntry.put(key, val);
        verify(bucket3).put(key, val);
    }

    @Test
    public void shouldCreateBucket() {
        testedEntry.setBuckets(new Map[CAPACITY]);
        doReturn(bucket1).when(testedEntry).createBucket();
        testedEntry.put(key, val);
        assertThat(testedEntry.getBuckets()[1])
                .isNotNull()
                .isSameAs(bucket1);
        verify(bucket1).put(key, val);
    }

    @Test
    public void shouldReturnValueFromBucket1() {
        key = 3;
        doReturn(val).when(bucket1).get(key);
        assertThat(testedEntry.get(key)).isSameAs(val);
    }

    @Test
    public void shouldReturnValueFromBucket2() {
        key = 1;
        doReturn(val).when(bucket2).get(key);
        assertThat(testedEntry.get(key)).isSameAs(val);
    }

    @Test
    public void shouldReturnValueFromBucket3() {
        key = 2;
        doReturn(val).when(bucket3).get(key);
        assertThat(testedEntry.get(key)).isSameAs(val);
    }

    @Test
    public void shouldReturnNullWhenBucketIsAbsent() {
        testedEntry.setBuckets(new Map[CAPACITY]);
        assertThat(testedEntry.get(key)).isNull();
    }

    private Object createVal() {
        return new Object();
    }
}